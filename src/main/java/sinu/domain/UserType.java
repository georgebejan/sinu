package sinu.domain;

/**
 * @author George Bejan
 */
public enum UserType {
    STUDENT, TEACHER, ADMIN
}
