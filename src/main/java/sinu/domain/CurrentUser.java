package sinu.domain;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author George Bejan
 */
public class CurrentUser extends org.springframework.security.core.userdetails.User {
    private final Integer id;
    private final UserType userType;

    public CurrentUser(final String username,
                       final String password,
                       final Collection<? extends GrantedAuthority> authorities,
                       final Integer userId,
                       final UserType userType) {
        super(username, password, authorities);
        this.id = userId;
        this.userType = userType;
    }

    public UserType getUserType() {
        return userType;
    }

    public Integer getId() {
        return id;
    }
}
