package sinu.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * @author George Bejan
 */
@Entity
public class Enrollment extends EntityWithId {
    private Student student;
    private Course course;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
