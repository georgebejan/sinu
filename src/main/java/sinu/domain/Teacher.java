package sinu.domain;

import javax.persistence.*;
import java.util.List;

/**
 * @author George Bejan
 */
@Entity
public class Teacher extends EntityWithId {
    private Person person;
    private User user;
    private List<Course> courses;

    @OneToOne
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @OneToOne
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher", cascade = CascadeType.ALL)
    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
