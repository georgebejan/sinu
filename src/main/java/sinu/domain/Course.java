package sinu.domain;

import javax.persistence.*;
import java.util.List;

/**
 * @author George Bejan
 */
@Entity
public class Course extends EntityWithId {
    private String subject;
    private Teacher teacher;
    private String webpage;
    private List<Teacher> labTeachers;
    private List<Enrollment> enrollments;

    public String getWebpage() {
        return webpage;
    }

    public void setWebpage(String webpage) {
        this.webpage = webpage;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "course_lab_teachers", foreignKey = @ForeignKey(name = "FK_course_lab_teachers"))
    public List<Teacher> getLabTeachers() {
        return labTeachers;
    }

    public void setLabTeachers(List<Teacher> labTeachers) {
        this.labTeachers = labTeachers;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "course", cascade = CascadeType.ALL)
    public List<Enrollment> getEnrollments() {
        return enrollments;
    }

    public void setEnrollments(List<Enrollment> enrollments) {
        this.enrollments = enrollments;
    }
}
