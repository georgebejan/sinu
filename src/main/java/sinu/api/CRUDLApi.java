package sinu.api;

import java.util.List;

/**
 * @author George Bejan
 */
public interface CRUDLApi<DTO> {
    List<DTO> list();

    DTO get(Integer id);

    void delete(Integer id);

    DTO save(DTO entity);
}
