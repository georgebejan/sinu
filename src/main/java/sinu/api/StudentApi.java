package sinu.api;

import sinu.dto.CourseDTO;
import sinu.dto.StudentDTO;

import java.util.List;

/**
 * @author George Bejan
 */
public interface StudentApi extends CRUDLApi<StudentDTO> {
    List<CourseDTO> getCourses(final Integer studentId);
}
