package sinu.api;

import sinu.dto.EnrollmentDTO;

/**
 * @author George Bejan
 */
public interface EnrollmentApi extends CRUDLApi<EnrollmentDTO> {
}
