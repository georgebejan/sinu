package sinu.api;

import sinu.dto.CourseDTO;
import sinu.dto.StudentDTO;

import java.util.List;

/**
 * @author George Bejan
 */
public interface CourseApi extends CRUDLApi<CourseDTO> {
    List<StudentDTO> getStudents(final Integer courseId);
}
