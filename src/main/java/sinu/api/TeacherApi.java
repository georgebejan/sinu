package sinu.api;

import sinu.dto.CourseDTO;
import sinu.dto.TeacherDTO;

import java.util.List;

/**
 * @author George Bejan
 */
public interface TeacherApi extends CRUDLApi<TeacherDTO> {
    List<CourseDTO> getCourses(final Integer teacherId);
}
