package sinu.api;

import sinu.domain.User;
import sinu.dto.AccountDTO;

/**
 * @author George Bejan
 */
public interface UserApi {
    User createAccount(AccountDTO dto);
}
