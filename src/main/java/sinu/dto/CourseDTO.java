package sinu.dto;

import java.util.List;

/**
 * @author George Bejan
 */
public class CourseDTO extends BaseDTO {
    private String subject;
    private TeacherDTO teacher;
    private String webpage;
    private List<TeacherDTO> labTeachers;

    public String getWebpage() {
        return webpage;
    }

    public void setWebpage(String webpage) {
        this.webpage = webpage;
    }

    public List<TeacherDTO> getLabTeachers() {
        return labTeachers;
    }

    public void setLabTeachers(List<TeacherDTO> labTeachers) {
        this.labTeachers = labTeachers;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public TeacherDTO getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherDTO teacher) {
        this.teacher = teacher;
    }
}
