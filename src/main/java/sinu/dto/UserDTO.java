package sinu.dto;

import sinu.domain.UserType;

/**
 * @author George Bejan
 */
public class UserDTO extends BaseDTO {
    private String username;
    private UserType userType;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
