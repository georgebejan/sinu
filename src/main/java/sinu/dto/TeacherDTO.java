package sinu.dto;

/**
 * @author George Bejan
 */
public class TeacherDTO extends BaseDTO {
    private PersonDTO person;
    private UserDTO user;

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}
