package sinu.dto;

/**
 * @author George Bejan
 */
public class StudentDTO extends BaseDTO {
    private String CNP;
    private PersonDTO person;
    private UserDTO user;

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}
