package sinu.dto;

/**
 * @author George Bejan
 */
public class EnrollmentDTO extends BaseDTO {
    private StudentDTO student;
    private CourseDTO course;

    public StudentDTO getStudent() {
        return student;
    }

    public void setStudent(StudentDTO student) {
        this.student = student;
    }

    public CourseDTO getCourse() {
        return course;
    }

    public void setCourse(CourseDTO course) {
        this.course = course;
    }
}
