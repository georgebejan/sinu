package sinu.dto;

/**
 * @author George Bejan
 */
public class BaseDTO {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
