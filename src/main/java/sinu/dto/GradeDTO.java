package sinu.dto;

/**
 * @author George Bejan
 */
public class GradeDTO extends BaseDTO {
    private Integer value;
    private EnrollmentDTO enrollment;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public EnrollmentDTO getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(EnrollmentDTO enrollment) {
        this.enrollment = enrollment;
    }
}
