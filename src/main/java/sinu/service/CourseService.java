package sinu.service;

import sinu.api.CourseApi;
import sinu.domain.Course;
import sinu.dto.CourseDTO;

/**
 * @author George Bejan
 */
public interface CourseService extends CourseApi, CRUDLService<CourseDTO, Course> {
}
