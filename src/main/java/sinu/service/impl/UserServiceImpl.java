package sinu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sinu.domain.User;
import sinu.domain.UserType;
import sinu.dto.AccountDTO;
import sinu.dto.UserDTO;
import sinu.repository.UserRepository;
import sinu.service.AbstractCRUDLService;
import sinu.service.SecurityService;
import sinu.service.UserService;

/**
 * @author George Bejan
 */
@Service
public class UserServiceImpl extends AbstractCRUDLService<UserDTO, User> implements UserService, UserDetailsService {
    @Autowired
    private UserRepository repository;
    @Autowired
    private SecurityService securityService;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = repository.findOneByUsername(username);

        return securityService.createCurrentUserObject(user.getId(), user.getUsername(), user.getPassword(), user.getType());
    }

    @Override
    public User createAccount(AccountDTO dto) {
        final User user = new User();
        user.setPassword(dto.getPassword());
        user.setUsername(dto.getUsername());
        user.setType(UserType.STUDENT);
        return repository.save(user);
    }

    @Override
    protected User updateEntity(User user, UserDTO dto) {
        user.setType(dto.getUserType());
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        return user;
    }
}
