package sinu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sinu.domain.Enrollment;
import sinu.dto.EnrollmentDTO;
import sinu.service.AbstractCRUDLService;
import sinu.service.CourseService;
import sinu.service.EnrollmentService;
import sinu.service.StudentService;

/**
 * @author George Bejan
 */
@Service
public class EnrollmentServiceImpl extends AbstractCRUDLService<EnrollmentDTO, Enrollment> implements EnrollmentService {
    @Autowired
    private StudentService studentService;
    @Autowired
    private CourseService courseService;

    @Override
    protected Enrollment updateEntity(Enrollment enrollment, EnrollmentDTO dto) {
        setReference(studentService, dto.getStudent(), enrollment.getStudent(), enrollment::setStudent);
        setReference(courseService, dto.getCourse(), enrollment.getCourse(), enrollment::setCourse);
        return enrollment;
    }
}
