package sinu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sinu.domain.Student;
import sinu.domain.UserType;
import sinu.dto.*;
import sinu.repository.StudentRepository;
import sinu.service.AbstractCRUDLService;
import sinu.service.PersonService;
import sinu.service.StudentService;
import sinu.service.UserService;
import sinu.service.converter.EnrollmentDTOConverter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author George Bejan
 */
@Service
public class StudentServiceImpl extends AbstractCRUDLService<StudentDTO, Student> implements StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private EnrollmentDTOConverter enrollmentDTOConverter;
    @Autowired
    private PersonService personService;
    @Autowired
    private UserService userService;

    @Override
    protected Student updateEntity(Student student, StudentDTO dto) {
        student.setCNP(dto.getCNP());
        PersonDTO person = dto.getPerson();
        UserDTO user = dto.getUser();
        if (student.getId() == null) {
            if (user == null) user = new UserDTO();
            user.setUserType(UserType.STUDENT);
            final String username = dto.getPerson().getLastName().toLowerCase() + "_" + dto.getPerson().getFirstName().toLowerCase();
            user.setUsername(username);
            user.setPassword(dto.getCNP());
            user = userService.save(user);
            person = personService.save(dto.getPerson());
        }
        setReference(personService, person, student.getPerson(), student::setPerson);
        setReference(userService, user, student.getUser(), student::setUser);
        return student;
    }

    @Override
    public List<CourseDTO> getCourses(final Integer userId) {
        final Student student = studentRepository.findByUserId(userId);
        return student.getEnrollments().stream().map(e -> enrollmentDTOConverter.convert(e))
                .map(EnrollmentDTO::getCourse).collect(Collectors.toList());
    }
}
