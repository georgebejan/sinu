package sinu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sinu.domain.Course;
import sinu.dto.CourseDTO;
import sinu.dto.EnrollmentDTO;
import sinu.dto.StudentDTO;
import sinu.service.AbstractCRUDLService;
import sinu.service.CourseService;
import sinu.service.TeacherService;
import sinu.service.converter.EnrollmentDTOConverter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author George Bejan
 */
@Service
public class CourseServiceImpl extends AbstractCRUDLService<CourseDTO, Course> implements CourseService {
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private EnrollmentDTOConverter enrollmentDTOConverter;

    @Override
    protected Course updateEntity(Course course, CourseDTO dto) {
        course.setSubject(dto.getSubject());
        setReference(teacherService, dto.getTeacher(), course.getTeacher(), course::setTeacher);
        setReferences(teacherService, dto.getLabTeachers(), course.getLabTeachers(), course::setLabTeachers);
        course.setWebpage(dto.getWebpage());
        return course;
    }

    @Override
    public List<StudentDTO> getStudents(Integer courseId) {
        final Course course = getEntity(courseId);
        return course.getEnrollments().stream().map(e -> enrollmentDTOConverter.convert(e))
                .map(EnrollmentDTO::getStudent).collect(Collectors.toList());
    }
}
