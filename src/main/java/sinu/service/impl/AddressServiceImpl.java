package sinu.service.impl;

import org.springframework.stereotype.Service;
import sinu.domain.Address;
import sinu.dto.AddressDTO;
import sinu.service.AbstractCRUDLService;
import sinu.service.AddressService;

/**
 * @author George Bejan
 */
@Service
public class AddressServiceImpl extends AbstractCRUDLService<AddressDTO, Address> implements AddressService {
    @Override
    protected Address updateEntity(Address address, AddressDTO dto) {
        address.setStreetName(dto.getStreetName());
        address.setNumber(dto.getNumber());
        address.setCity(dto.getCity());
        address.setState(dto.getState());
        address.setCountry(dto.getCountry());
        return address;
    }
}
