package sinu.service.impl;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import sinu.domain.CurrentUser;
import sinu.domain.UserType;
import sinu.service.SecurityService;

import java.util.Collections;

/**
 * @author George Bejan
 */
@Service
public class SpringSecurityService implements SecurityService {
    @Override
    public CurrentUser getCurrentUser() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication auth = securityContext.getAuthentication();
        //getAuthentication may return null if no auth info is available
        if (auth != null) {
            final Object principal = auth.getPrincipal();
            //getPrincipal returns a string object for anonymous users
            if (principal instanceof CurrentUser) {
                return (CurrentUser) principal;
            }
        }
        return null;
    }

    @Override
    public CurrentUser createCurrentUserObject(int userId, String username, String password, UserType type) {
        return new CurrentUser(username, password, Collections.emptyList(), userId, type);
    }
}
