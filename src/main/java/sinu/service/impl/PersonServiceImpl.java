package sinu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sinu.domain.Person;
import sinu.dto.AddressDTO;
import sinu.dto.PersonDTO;
import sinu.service.AbstractCRUDLService;
import sinu.service.AddressService;
import sinu.service.PersonService;

/**
 * @author George Bejan
 */
@Service
public class PersonServiceImpl extends AbstractCRUDLService<PersonDTO, Person> implements PersonService {
    @Autowired
    private AddressService addressService;

    @Override
    protected Person updateEntity(Person person, PersonDTO dto) {
        person.setFirstName(dto.getFirstName());
        person.setLastName(dto.getLastName());
        AddressDTO address = dto.getAddress();
        if (person.getId() == null) {
            address = addressService.save(dto.getAddress());
        }
        setReference(addressService, address, person.getAddress(), person::setAddress);

        return person;
    }
}
