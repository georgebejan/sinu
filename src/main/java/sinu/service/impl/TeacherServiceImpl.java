package sinu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sinu.domain.Teacher;
import sinu.domain.UserType;
import sinu.dto.CourseDTO;
import sinu.dto.PersonDTO;
import sinu.dto.TeacherDTO;
import sinu.dto.UserDTO;
import sinu.repository.TeacherRepository;
import sinu.service.AbstractCRUDLService;
import sinu.service.PersonService;
import sinu.service.TeacherService;
import sinu.service.UserService;
import sinu.service.converter.CourseDTOConverter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author George Bejan
 */
@Service
public class TeacherServiceImpl extends AbstractCRUDLService<TeacherDTO, Teacher> implements TeacherService {
    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private CourseDTOConverter courseDTOConverter;
    @Autowired
    private UserService userService;
    @Autowired
    private PersonService personService;

    @Override
    protected Teacher updateEntity(Teacher teacher, TeacherDTO dto) {
        PersonDTO person = dto.getPerson();
        UserDTO user = dto.getUser();
        if (teacher.getId() == null) {
            if (user == null) user = new UserDTO();
            user.setUserType(UserType.TEACHER);
            final String username = dto.getPerson().getLastName().toLowerCase() + "_" + dto.getPerson().getFirstName().toLowerCase();
            user.setUsername(username);
            user.setPassword(username);
            user = userService.save(user);
            person = personService.save(dto.getPerson());
        }
        setReference(personService, person, teacher.getPerson(), teacher::setPerson);
        setReference(userService, user, teacher.getUser(), teacher::setUser);
        return teacher;
    }

    @Override
    public List<CourseDTO> getCourses(Integer userId) {
        final Teacher teacher = teacherRepository.findByUserId(userId);
        return teacher.getCourses().stream().map(c -> courseDTOConverter.convert(c)).collect(Collectors.toList());
    }
}
