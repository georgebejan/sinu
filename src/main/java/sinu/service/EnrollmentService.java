package sinu.service;

import sinu.api.EnrollmentApi;
import sinu.domain.Enrollment;
import sinu.dto.EnrollmentDTO;

/**
 * @author George Bejan
 */
public interface EnrollmentService extends EnrollmentApi, CRUDLService<EnrollmentDTO, Enrollment> {
}
