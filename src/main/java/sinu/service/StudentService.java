package sinu.service;

import sinu.api.StudentApi;
import sinu.domain.Student;
import sinu.dto.StudentDTO;

/**
 * @author George Bejan
 */
public interface StudentService extends StudentApi, CRUDLService<StudentDTO, Student> {
}
