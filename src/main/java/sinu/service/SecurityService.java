package sinu.service;

import sinu.domain.CurrentUser;
import sinu.domain.UserType;

/**
 * @author George Bejan
 */
public interface SecurityService {
    CurrentUser getCurrentUser();

    CurrentUser createCurrentUserObject(int userId, String username, String password, UserType type);
}
