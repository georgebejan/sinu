package sinu.service;

import sinu.domain.Person;
import sinu.dto.PersonDTO;

/**
 * @author George Bejan
 */
public interface PersonService extends CRUDLService<PersonDTO, Person> {
}
