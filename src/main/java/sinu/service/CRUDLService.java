package sinu.service;

/**
 * @author George Bejan
 */
public interface CRUDLService<DTO, ENTITY> {
    DTO save(DTO dto);

    ENTITY getEntity(Integer id);


    Iterable<ENTITY> getEntities(Iterable<Integer> id);


    ENTITY newEntity();

}
