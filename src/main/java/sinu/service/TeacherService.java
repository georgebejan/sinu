package sinu.service;

import sinu.api.TeacherApi;
import sinu.domain.Teacher;
import sinu.dto.TeacherDTO;

/**
 * @author George Bejan
 */
public interface TeacherService extends TeacherApi, CRUDLService<TeacherDTO, Teacher> {
}
