package sinu.service;

import sinu.api.UserApi;
import sinu.domain.User;
import sinu.dto.UserDTO;

/**
 * @author George Bejan
 */
public interface UserService extends UserApi, CRUDLService<UserDTO, User> {
}
