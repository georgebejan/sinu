package sinu.service;

import sinu.domain.Address;
import sinu.dto.AddressDTO;

/**
 * @author George Bejan
 */
public interface AddressService extends CRUDLService<AddressDTO, Address> {
}
