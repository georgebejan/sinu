package sinu.service.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sinu.domain.Course;
import sinu.dto.CourseDTO;

import java.util.stream.Collectors;

/**
 * @author George Bejan
 */
@Component
public class CourseDTOConverter extends AbstractConverter<CourseDTO, Course> {
    @Autowired
    private TeacherDTOConverter teacherDTOConverter;

    @Override
    public CourseDTO convert(Course source) {
        final CourseDTO dto = new CourseDTO();
        dto.setSubject(source.getSubject());
        dto.setTeacher(teacherDTOConverter.convert(source.getTeacher()));
        dto.setWebpage(source.getWebpage());
        dto.setLabTeachers(source.getLabTeachers().stream()
                .map(t -> teacherDTOConverter.convert(t)).collect(Collectors.toList()));
        return super.convert(source, dto);
    }
}
