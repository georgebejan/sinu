package sinu.service.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sinu.domain.Enrollment;
import sinu.dto.EnrollmentDTO;

/**
 * @author George Bejan
 */
@Component
public class EnrollmentDTOConverter extends AbstractConverter<EnrollmentDTO, Enrollment> {
    @Autowired
    private StudentDTOConverter studentDTOConverter;
    @Autowired
    private CourseDTOConverter courseDTOConverter;

    @Override
    public EnrollmentDTO convert(Enrollment source) {
        final EnrollmentDTO dto = new EnrollmentDTO();
        dto.setCourse(courseDTOConverter.convert(source.getCourse()));
        dto.setStudent(studentDTOConverter.convert(source.getStudent()));
        return super.convert(source, dto);
    }
}
