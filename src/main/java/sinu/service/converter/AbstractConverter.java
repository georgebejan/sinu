package sinu.service.converter;

import sinu.domain.EntityWithId;
import sinu.dto.BaseDTO;

/**
 * @author George Bejan
 */
public abstract class AbstractConverter<DTO extends BaseDTO, ENTITY extends EntityWithId> {
    public abstract DTO convert(final ENTITY source);

    public DTO convert(final ENTITY source, final DTO dto) {
        if (source == null || dto == null) {
            return null;
        }

        dto.setId(source.getId());
        return dto;
    }
}

