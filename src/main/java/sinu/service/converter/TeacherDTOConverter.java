package sinu.service.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sinu.domain.Teacher;
import sinu.dto.TeacherDTO;

/**
 * @author George Bejan
 */
@Component
public class TeacherDTOConverter extends AbstractConverter<TeacherDTO, Teacher> {
    @Autowired
    private PersonDTOConverter personDTOConverter;
    @Autowired
    private UserDTOConverter userDTOConverter;

    @Override
    public TeacherDTO convert(Teacher source) {
        final TeacherDTO dto = new TeacherDTO();
        dto.setPerson(personDTOConverter.convert(source.getPerson()));
        dto.setUser(userDTOConverter.convert(source.getUser()));
        return super.convert(source, dto);
    }
}
