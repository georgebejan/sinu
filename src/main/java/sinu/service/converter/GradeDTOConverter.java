package sinu.service.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sinu.domain.Grade;
import sinu.dto.GradeDTO;

/**
 * @author George Bejan
 */
@Component
public class GradeDTOConverter extends AbstractConverter<GradeDTO, Grade> {
    @Autowired
    private EnrollmentDTOConverter enrollmentDTOConverter;

    @Override
    public GradeDTO convert(Grade source) {
        final GradeDTO dto = new GradeDTO();
        dto.setValue(source.getValue());
        dto.setEnrollment(enrollmentDTOConverter.convert(source.getEnrollment()));
        return super.convert(source, dto);
    }
}
