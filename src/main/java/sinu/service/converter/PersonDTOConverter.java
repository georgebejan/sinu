package sinu.service.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sinu.domain.Person;
import sinu.dto.PersonDTO;

/**
 * @author George Bejan
 */
@Component
public class PersonDTOConverter extends AbstractConverter<PersonDTO, Person> {
    @Autowired
    private AddressDTOConverter addressDTOConverter;

    @Override
    public PersonDTO convert(Person source) {
        final PersonDTO dto = new PersonDTO();
        dto.setFirstName(source.getFirstName());
        dto.setLastName(source.getLastName());
        dto.setAddress(addressDTOConverter.convert(source.getAddress()));
        return super.convert(source, dto);
    }
}
