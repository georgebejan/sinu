package sinu.service.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sinu.domain.Student;
import sinu.dto.StudentDTO;

/**
 * @author George Bejan
 */
@Component
public class StudentDTOConverter extends AbstractConverter<StudentDTO, Student> {
    @Autowired
    private PersonDTOConverter personDTOConverter;
    @Autowired
    private UserDTOConverter userDTOConverter;

    @Override
    public StudentDTO convert(Student source) {
        final StudentDTO dto = new StudentDTO();
        dto.setCNP(source.getCNP());
        dto.setPerson(personDTOConverter.convert(source.getPerson()));
        dto.setUser(userDTOConverter.convert(source.getUser()));
        return super.convert(source, dto);
    }
}
