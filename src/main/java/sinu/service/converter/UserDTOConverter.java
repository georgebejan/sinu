package sinu.service.converter;

import org.springframework.stereotype.Component;
import sinu.domain.User;
import sinu.dto.UserDTO;

/**
 * @author George Bejan
 */
@Component
public class UserDTOConverter extends AbstractConverter<UserDTO, User> {
    @Override
    public UserDTO convert(User source) {
        final UserDTO dto = new UserDTO();
        if (source != null) {
            dto.setUsername(source.getUsername());
            dto.setUserType(source.getType());
            return convert(source, dto);
        }
        return dto;
    }
}
