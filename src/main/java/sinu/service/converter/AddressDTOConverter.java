package sinu.service.converter;

import org.springframework.stereotype.Component;
import sinu.domain.Address;
import sinu.dto.AddressDTO;

/**
 * @author George Bejan
 */
@Component
public class AddressDTOConverter extends AbstractConverter<AddressDTO, Address> {
    @Override
    public AddressDTO convert(Address source) {
        final AddressDTO dto = new AddressDTO();
        dto.setCity(source.getCity());
        dto.setCountry(source.getCountry());
        dto.setNumber(source.getNumber());
        dto.setState(source.getState());
        dto.setStreetName(source.getStreetName());
        return super.convert(source, dto);
    }
}
