package sinu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import sinu.domain.EntityWithId;
import sinu.dto.BaseDTO;
import sinu.repository.SinuRepository;
import sinu.service.converter.AbstractConverter;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author George Bejan
 */
public abstract class AbstractCRUDLService<DTO extends BaseDTO, ENTITY extends EntityWithId>
        implements CRUDLService<DTO, ENTITY> {
    @Autowired
    private SinuRepository<ENTITY> repository;
    @Autowired
    private AbstractConverter<DTO, ENTITY> conversionService;
    private final Class<ENTITY> entityType;

    protected AbstractCRUDLService() {
        final Class<? extends AbstractCRUDLService> aClass = getClass();
        final Class<?>[] localParameters = GenericTypeResolver.resolveTypeArguments(aClass, AbstractCRUDLService.class);
        this.entityType = (Class<ENTITY>) localParameters[1];
    }

    protected abstract ENTITY updateEntity(ENTITY entity, DTO dto);

    public final List<DTO> list() {
        final Iterable<ENTITY> all = repository.findAll();
        final List<DTO> result = new ArrayList<>();
        all.forEach(e -> result.add(conversionService.convert(e)));
        return result;
    }

    public final DTO get(final Integer id) {
        final ENTITY one = getEntity(id);
        return conversionService.convert(one);
    }

    public final void delete(final Integer id) {
        repository.delete(id);
    }

    @Override
    public final DTO save(final DTO dto) {
        final boolean isNewEntity = isNewEntity(dto);
        ENTITY entity = isNewEntity ? newEntity() : getEntity(dto.getId());
        entity = updateEntity(entity, dto);
        repository.save(entity);
        return conversionService.convert(entity);
    }

    @Override
    public ENTITY getEntity(Integer id) {
        return repository.findOne(id);
    }

    @Override
    public ENTITY newEntity() {
        try {
            return entityType.newInstance();
        } catch (final InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Iterable<ENTITY> getEntities(Iterable<Integer> id) {
        return repository.findAll(id);
    }

    protected <R extends EntityWithId, D extends BaseDTO> boolean setReference(
            final CRUDLService<?, R> service,
            final D newReferenceDTO,
            final R existingReference,
            final Consumer<R> referenceSetter) {

        return this.setReference(
                service::getEntity,
                newReferenceDTO,
                existingReference,
                referenceSetter);
    }

    protected <R extends EntityWithId, D extends BaseDTO> boolean setReferences(
            final CRUDLService<?, R> service,
            final Collection<D> newReferenceDTOs,
            final Collection<R> existingReferences, final Consumer<List<R>> referenceSetter) {
        // both are null -> do nothing
        if (existingReferences == null && newReferenceDTOs == null) {
            return false;
        }

        // setting the reference explicitly to null
        if (newReferenceDTOs == null) {
            referenceSetter.accept(null);
            return true;
        }

        // IDs are the same -> do nothing
        if (existingReferences != null && listEquals(newReferenceDTOs, existingReferences)) {
            return false;
        }

        // setting a new reference
        final Iterable<R> newReferences = service.getEntities(newReferenceDTOs.stream().map(BaseDTO::getId).collect(Collectors.toList()));
        final List<R> result = new ArrayList<>();
        for (R newReference : newReferences) {
            result.add(newReference);
        }

        // finally accept the new reference
        referenceSetter.accept(result);
        return true;
    }

    private <R extends EntityWithId, D extends BaseDTO> boolean listEquals(
            final Collection<D> newReferenceDTOs,
            final Collection<R> existingReferences) {
        // both null -> ok
        if (newReferenceDTOs == null && existingReferences == null) {
            return true;
        }
        // one null -> not ok
        if (newReferenceDTOs == null || existingReferences == null) {
            return false;
        }

        final int n = newReferenceDTOs.size();

        // different size
        if (n != existingReferences.size()) {
            return false;
        }

        final List<D> newReferenceDTOsList = new ArrayList<>(newReferenceDTOs);
        final List<R> existingReferencesList = new ArrayList<>(existingReferences);

        sortList(newReferenceDTOsList, BaseDTO::getId);
        sortList(existingReferencesList, EntityWithId::getId);

        for (int i = 0; i < n; ++i) {
            final D newReferenceDTO = newReferenceDTOsList.get(i);
            final R existingReference = existingReferencesList.get(i);

            // one null -> not ok
            if ((newReferenceDTO == null && existingReference != null)
                    || (existingReference == null && newReferenceDTO != null)) {
                return false;
            }

            // different IDs -> not ok
            if (existingReference != null && !existingReference.getId().equals(newReferenceDTO
                    .getId())) {
                return false;
            }
        }

        return true;
    }

    private <T> void sortList(final List<T> list, final Function<T, Integer> idGetter) {
        list.sort((a, b) -> {
            if (a == null && b != null) {
                return -1;
            }
            if ((a != null && b == null) || a == null) {
                return 1;
            }
            return idGetter.apply(a).compareTo(idGetter.apply(b));
        });
    }

    protected <R extends EntityWithId, D extends BaseDTO> boolean setReference(
            final Function<Integer, R> findEntityById,
            final D newReferenceDTO,
            final R existingReference,
            final Consumer<R> referenceSetter) {

        // both are null -> do nothing
        if (existingReference == null && newReferenceDTO == null) {
            return false;
        }

        // setting the reference explicitly to null
        if (newReferenceDTO == null) {
            referenceSetter.accept(null);
            return true;
        }

        // IDs are the same -> do nothing
        if (existingReference != null && existingReference.getId().equals(newReferenceDTO.getId()
        )) {
            return false;
        }

        // setting a new reference
        final R newReference = findEntityById.apply(newReferenceDTO.getId());

        // entity not found or no rights to view it
        if (newReference == null) {
            throw new EntityNotFoundException("entity with id " + newReferenceDTO.getId()
                    + " is not available or invisible");
        }

        // finally accept the new reference
        referenceSetter.accept(newReference);
        return true;
    }

    private boolean isNewEntity(DTO dto) {
        return dto.getId() == null;
    }
}

