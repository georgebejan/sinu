package sinu.repository;

import org.springframework.data.jpa.repository.Query;
import sinu.domain.Teacher;

/**
 * @author George Bejan
 */
public interface TeacherRepository extends SinuRepository<Teacher> {
    @Query(value = "SELECT * FROM TEACHER WHERE USER_ID = ?1", nativeQuery = true)
    Teacher findByUserId(final Integer userId);
}
