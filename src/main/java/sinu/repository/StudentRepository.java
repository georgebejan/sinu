package sinu.repository;

import org.springframework.data.jpa.repository.Query;
import sinu.domain.Student;

/**
 * @author George Bejan
 */
public interface StudentRepository extends SinuRepository<Student> {
    @Query(value = "SELECT * FROM STUDENT WHERE USER_ID = ?1", nativeQuery = true)
    Student findByUserId(Integer id);
}
