package sinu.repository;

import sinu.domain.Enrollment;

/**
 * @author George Bejan
 */
public interface EnrollmentRepository extends SinuRepository<Enrollment> {
}
