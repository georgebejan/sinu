package sinu.repository;

import sinu.domain.Course;

/**
 * @author George Bejan
 */
public interface CourseRepository extends SinuRepository<Course> {
}
