package sinu.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * @author George Bejan
 */
@NoRepositoryBean
public interface SinuRepository<ENTITY> extends JpaRepository<ENTITY, Integer> {
    List<ENTITY> findAll();

    ENTITY findOne(final Integer id);

    void delete(final Integer id);
}
