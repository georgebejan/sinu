package sinu.repository;

import sinu.domain.User;

/**
 * @author George Bejan
 */
public interface UserRepository extends SinuRepository<User> {
    User findOneByUsername(final String name);
}
