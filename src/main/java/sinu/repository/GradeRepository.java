package sinu.repository;

import sinu.domain.Grade;

/**
 * @author George Bejan
 */
public interface GradeRepository extends SinuRepository<Grade> {
}
