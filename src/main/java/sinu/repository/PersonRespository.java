package sinu.repository;

import sinu.domain.Person;

/**
 * @author George Bejan
 */
public interface PersonRespository extends SinuRepository<Person> {
}
