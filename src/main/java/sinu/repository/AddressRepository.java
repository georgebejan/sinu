package sinu.repository;

import sinu.domain.Address;

/**
 * @author George Bejan
 */
public interface AddressRepository extends SinuRepository<Address> {
}
