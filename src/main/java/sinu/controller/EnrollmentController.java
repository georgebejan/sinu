package sinu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sinu.api.EnrollmentApi;
import sinu.dto.EnrollmentDTO;

/**
 * @author George Bejan
 */
@RestController
@RequestMapping("api/enrollments")
public class EnrollmentController extends AbstractCRUDLRestController<EnrollmentDTO> {
    @Autowired
    protected EnrollmentController(EnrollmentApi api) {
        super(api);
    }
}
