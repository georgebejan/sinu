package sinu.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sinu.dto.BaseDTO;

import java.util.List;

/**
 * @author George Bejan
 */
public abstract class CRUDLRestController<DTO extends BaseDTO> {
    @RequestMapping(value = "", method = RequestMethod.GET)
    public abstract List<DTO> list();

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public abstract DTO get(final Integer id);

    @RequestMapping(value = "", method = RequestMethod.POST)
    public abstract DTO create(final DTO dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public abstract DTO update(@PathVariable("id") final Integer id, final DTO dto);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public abstract void delete(@PathVariable("id") final Integer id);
}
