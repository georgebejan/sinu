package sinu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sinu.domain.CurrentUser;
import sinu.service.SecurityService;

/**
 * @author George Bejan
 */
@RestController
@RequestMapping("api/account")
public class AccountController {
    @Autowired
    private SecurityService securityService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public CurrentUser account() {
        return securityService.getCurrentUser();
    }
}
