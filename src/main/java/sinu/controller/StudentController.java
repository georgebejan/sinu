package sinu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sinu.api.StudentApi;
import sinu.dto.CourseDTO;
import sinu.dto.StudentDTO;

import java.util.List;

/**
 * @author George Bejan
 */
@RestController
@RequestMapping("api/students")
public class StudentController extends AbstractCRUDLRestController<StudentDTO> {
    private StudentApi studentApi;

    @Autowired
    protected StudentController(final StudentApi api) {
        super(api);
        studentApi = api;
    }

    @RequestMapping(value = "{studentId}/courses", method = RequestMethod.GET)
    public List<CourseDTO> getCourses(@PathVariable("studentId") final Integer studentId) {
        return studentApi.getCourses(studentId);
    }
}
