package sinu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import sinu.api.UserApi;
import sinu.domain.CurrentUser;
import sinu.domain.UserType;
import sinu.dto.AccountDTO;
import sinu.service.SecurityService;

import javax.servlet.http.HttpServletRequest;

/**
 * @author George Bejan
 */
@Controller
public class MainController {
    private UserApi userApi;
    private SecurityService securityService;

    @Autowired
    public MainController(final UserApi userApi, final SecurityService securityService) {
        this.userApi = userApi;
        this.securityService = securityService;
    }

    @RequestMapping("/")
    public ModelAndView main() {
        final CurrentUser currentUser = securityService.getCurrentUser();
        final UserType userType = currentUser == null ? null : currentUser.getUserType();
        return getMainModel(userType);
    }

    private ModelAndView getMainModel(final UserType type) {
        final ModelAndView main;
        switch (type) {
            case ADMIN:
                main = new ModelAndView("layout/admin");
                break;
            case STUDENT:
                main = new ModelAndView("layout/student");
                break;
            case TEACHER:
                main = new ModelAndView("layout/teacher");
                break;
            default:
                main = null;
        }

        return main;
    }

    @RequestMapping("/login")
    public String login(final HttpServletRequest request, final Model model) {
        Throwable throwable = (Throwable) request.getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        String errorMessage = null;
        if (throwable != null) {
            throwable = unwrapCause(throwable);
            errorMessage = throwable.getMessage();
        }
        model.addAttribute("errorMessage", errorMessage);
        return "login";
    }

    @RequestMapping(value = "/verify", method = RequestMethod.GET)
    public Object verify() {
        final CurrentUser currentUser = securityService.getCurrentUser();
        return "redirect:/";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signUp() {
        return "signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String createAccount(AccountDTO dto) {
        userApi.createAccount(dto);
        return "signup";
    }

    private Throwable unwrapCause(final Throwable throwable) {
        Throwable result = throwable;
        while (result.getCause() != null) {
            result = result.getCause();
        }
        return result;
    }
}
