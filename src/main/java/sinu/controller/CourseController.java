package sinu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sinu.api.CourseApi;
import sinu.dto.CourseDTO;
import sinu.dto.StudentDTO;

import java.util.List;

/**
 * @author George Bejan
 */
@RestController
@RequestMapping("api/courses")
public class CourseController extends AbstractCRUDLRestController<CourseDTO> {
    private CourseApi courseApi;

    @Autowired
    protected CourseController(final CourseApi api) {
        super(api);
        courseApi = api;
    }

    @RequestMapping(value = "{courseId}/students", method = RequestMethod.GET)
    public List<StudentDTO> getCourses(@PathVariable("courseId") final Integer courseId) {
        return courseApi.getStudents(courseId);
    }
}
