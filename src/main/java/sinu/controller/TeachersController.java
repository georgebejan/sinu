package sinu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sinu.api.TeacherApi;
import sinu.dto.CourseDTO;
import sinu.dto.TeacherDTO;

import java.util.List;

/**
 * @author George Bejan
 */
@RestController
@RequestMapping("api/teachers")
public class TeachersController extends AbstractCRUDLRestController<TeacherDTO> {
    private TeacherApi teacherApi;

    @Autowired
    protected TeachersController(TeacherApi api) {
        super(api);
        teacherApi = api;
    }

    @RequestMapping(value = "{teacherId}/courses", method = RequestMethod.GET)
    public List<CourseDTO> getCourses(@PathVariable("teacherId") final Integer teacherId) {
        return teacherApi.getCourses(teacherId);
    }
}
