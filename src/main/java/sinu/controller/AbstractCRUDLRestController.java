package sinu.controller;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import sinu.api.CRUDLApi;
import sinu.dto.BaseDTO;

import java.util.List;

/**
 * @author George Bejan
 */
public abstract class AbstractCRUDLRestController<DTO extends BaseDTO> extends CRUDLRestController<DTO> {
    private CRUDLApi<DTO> api;

    protected AbstractCRUDLRestController(CRUDLApi<DTO> api) {
        this.api = api;
    }

    @Override
    public List<DTO> list() {
        return api.list();
    }

    @Override
    @Transactional(readOnly = true)
    public DTO get(@PathVariable("id") Integer id) {
        return api.get(id);
    }

    @Override
    public DTO update(Integer id, @RequestBody DTO dto) {
        return api.save(dto);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public DTO create(@RequestBody DTO dto) {
        return api.save(dto);
    }

    @Override
    public void delete(Integer id) {
        api.delete(id);
    }
}
