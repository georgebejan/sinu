/**
 * Created by George Bejan on 12/29/2015.
 */
'use strict';
angular.module("sinu.admin", ['ngRoute', 'sinu.common'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/students', {
                templateUrl: 'html/student/student-list.html',
                controller: 'studentsController'
            })
            .when('/teachers', {
                templateUrl: 'html/teacher/teacher-list.html',
                controller: 'teachersController'
            })
    })
    .controller("adminController", function ($scope, $http) {
        $http.get('api/account').then(function (response) {
            $scope.user = response.data;
        });
    })
    .controller("studentsController", function ($scope, $http) {
        var getStudents = function () {
            $http.get('api/students').then(function (response) {
                $scope.students = response.data;
            });
        };

        var copyProperties = function (source) {
            return {
                id: source.id,
                cnp: source.cnp,
                person: source.person,
                user: source.user
            };
        };

        getStudents();
        $scope.new = function () {
            $scope.editDetail = {};
            $scope.detail = undefined;
        };

        $scope.cancelEdit = function () {
            $scope.detail = $scope.previousVersion;
            $scope.editDetail = undefined;
        };
        $scope.edit = function () {
            $scope.editDetail = copyProperties($scope.detail);
            $scope.previousVersion = $scope.detail;
            $scope.detail = undefined;
        };

        $scope.details = function (student) {
            $scope.courses = {};
            $scope.detail = student;
            $scope.editDetail = undefined;
            $http.get('api/students/' + student.user.id + '/courses').then(function (response) {
                $scope.courses = response.data;
            });
        };

        $scope.save = function () {
            $http.post('api/students', $scope.editDetail).then(function () {
                    $scope.detail = $scope.editDetail;
                    $scope.editDetail = undefined;
                    getStudents();
                },
                function (response) {
                    //TODO: handle errors
                });
        }
    })

    .controller("teachersController", function ($scope, $http) {
        var getTeachers = function () {
            $http.get('api/teachers').then(function (response) {
                $scope.teachers = response.data;
            });
        };
        getTeachers();

        var copyProperties = function (source) {
            return {
                id: source.id,
                person: source.person,
                user: source.user
            };
        };

        $scope.new = function () {
            $scope.editDetail = {};
            $scope.detail = undefined;
        };

        $scope.cancelEdit = function () {
            $scope.detail = $scope.previousVersion;
            $scope.editDetail = undefined;
        };
        $scope.edit = function () {
            $scope.editDetail = copyProperties($scope.detail);
            $scope.previousVersion = $scope.detail;
            $scope.detail = undefined;
        };

        $scope.details = function (teacher) {
            $scope.courses = {};
            $scope.detail = teacher;
            $scope.editDetail = undefined;
            $http.get('api/teachers/' + teacher.user.id + '/courses').then(function (response) {
                $scope.courses = response.data;
            });
        };

        $scope.save = function () {
            $http.post('api/teachers', $scope.editDetail).then(function () {
                    $scope.detail = $scope.editDetail;
                    $scope.editDetail = undefined;
                    getTeachers();
                },
                function (response) {
                    //TODO: handle errors
                });
        }
    });
