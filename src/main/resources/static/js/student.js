/**
 * Created by George Bejan on 12/29/2015.
 */
'use strict';
var sinu = angular.module("sinu.student", ['sinu.common'])
    .controller("studentController", function ($scope, $http) {
        $http.get('api/account').then(function (response) {
            $scope.user = response.data;
        });
    });
