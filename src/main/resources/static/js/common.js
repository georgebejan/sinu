/**
 * Created by George Bejan on 1/9/2016.
 */
angular.module("sinu.common", ['ngRoute'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/courses', {
                templateUrl: 'html/course/course-list.html',
                controller: 'coursesController'
            })
            .when('/', {
                redirectTo: '/courses'
            })

    })
    .controller("coursesController", function ($scope, $http) {
        var getCourses = function () {
            switch ($scope.user.userType) {
                case 'STUDENT':
                    $http.get('api/students/' + $scope.user.id + '/courses').then(function (response) {
                        $scope.courses = response.data;
                    });
                    break;
                case 'TEACHER':
                    $http.get('api/teachers/' + $scope.user.id + '/courses').then(function (response) {
                        $scope.courses = response.data;
                    });
                    break;
                case 'ADMIN':
                    $http.get('api/courses').then(function (response) {
                        $scope.courses = response.data;
                    });
                    break;
            }
        };

        $http.get('api/account').then(function (response) {
            $scope.user = response.data;
            $scope.isStudent = $scope.user.userType === 'STUDENT';
            $scope.isAdmin = $scope.user.userType === 'ADMIN';
            getCourses();
        });

        var getTeachers = function () {
            $http.get('api/teachers').then(function (response) {
                $scope.teachers = response.data;
            });
        };

        var copyProperties = function (source) {
            return {
                id: source.id,
                subject: source.subject,
                teacher: source.teacher,
                webpage: source.webpage,
                labTeachers: source.labTeachers
            };
        };

        $scope.details = function (course) {
            $scope.detail = course;
            $scope.editDetail = undefined;
            $http.get('api/courses/' + course.id + '/students').then(function (response) {
                $scope.students = response.data;
            });
        };

        $scope.edit = function () {
            getTeachers();
            $scope.editDetail = copyProperties($scope.detail);
            $scope.previousVersion = copyProperties($scope.detail);
            $scope.detail = undefined;

            $scope.editDetail.teacher = JSON.stringify($scope.editDetail.teacher);
        };

        $scope.cancelEdit = function () {
            $scope.detail = $scope.previousVersion;
            $scope.editDetail = undefined;
        };

        $scope.new = function () {
            getTeachers();
            $scope.editDetail = {};
            $scope.previousVersion = copyProperties($scope.detail);
            $scope.detail = undefined;
        };

        $scope.save = function () {
            $scope.editDetail.teacher = JSON.parse($scope.editDetail.teacher);
            for (var i = 0; i < $scope.editDetail.labTeachers.length; i++) {
                $scope.editDetail.labTeachers[i] = JSON.parse($scope.editDetail.labTeachers[i]);
            }

            $http.post('api/courses', $scope.editDetail).then(function () {
                    $scope.detail = $scope.editDetail;
                    $scope.editDetail = undefined;
                    getCourses();
                },
                function (response) {
                    //TODO: handle errors
                });
        };

        $scope.addToCourse = function () {
            $scope.add = {};
            $http.get('api/students/').then(function (response) {
                $scope.allStudents = response.data;
                $scope.showAddStudent = true;
            });
        };

        $scope.addStudentToCourse = function () {
            var enrollment = {
                course: $scope.detail,
                student: JSON.parse($scope.add.studentToAdd)
            };
            $http.post('api/enrollments', enrollment).then(function () {
                    $scope.showAddStudent = false;
                    $scope.add = {};
                    $http.get('api/courses/' + $scope.detail.id + '/students').then(function (response) {
                        $scope.students = response.data;
                    });
                },
                function (response) {
                    //TODO: handle errors
                });
        }
    });