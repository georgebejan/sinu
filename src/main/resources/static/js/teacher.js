/**
 * Created by George Bejan on 1/9/2016.
 */
/**
 * Created by George Bejan on 12/29/2015.
 */
'use strict';
var sinu = angular.module("sinu.teacher", ['sinu.common'])
    .controller("teacherController", function ($scope, $http) {
        $http.get('api/account').then(function (response) {
            $scope.user = response.data;
        });
    });
